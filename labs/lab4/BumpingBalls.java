package lab4;

import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {
	
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int noBalls = ap.nextInt("Enter number of balls: ");
		int pause = ap.nextInt("Enter pause time:");
		

		// set the scale of the coordinate system
		StdDraw.setXscale(-1.0, 1.0);
		StdDraw.setYscale(-1.0, 1.0);

		// initial values
		double[] balls = new double[noBalls];
		double[] rxBalls = new double[noBalls];
		double[] ryBalls = new double[noBalls];
		double[] vxBalls = new double[noBalls];
		double[] vyBalls = new double[noBalls];

		Color[] xColor = new Color[noBalls];


//initial position of the balls
		for(int i=0; i < noBalls; i++) {
			rxBalls[i]=Math.random()*((-Math.random()*1)+1);
			ryBalls[i]=Math.random()*((-Math.random()*1)+1);
			vxBalls[i]=0.05;
			vyBalls[i]=0.05; 
		}

		for(int i=0; i < noBalls; i++) {

			int x = (int) (Math.random()*255) + 1;
			int y = (int) (Math.random()*255) + 1;
			int z = (int) (Math.random()*255) + 1;
			xColor[i]= new Color(x,y,z);

		}
	
		double radius = 0.02;             

		
		while (true)  { 

			StdDraw.setPenColor(StdDraw.GRAY);
			StdDraw.filledSquare(0, 0, 1.06);

			
			for(int i = 0; i <noBalls; i++) {

				if (Math.abs(rxBalls[i] + vxBalls[i]) > 1.0 - radius) vxBalls[i] = -vxBalls[i];
				if (Math.abs(ryBalls[i] + vyBalls[i]) > 1.0 - radius) vyBalls[i] = -vyBalls[i];

				for(int j = 0; j < noBalls; j++) {
					if(j!=i) {
						double a = Math.pow((rxBalls[j]- rxBalls[i]), 2) + Math.pow((ryBalls[j]- ryBalls[i]), 2);
						double dist = Math.sqrt(a);
						if (dist <  2*radius) vxBalls[j] = -vxBalls[j];
						if (dist <  2*radius) vyBalls[j] = -vyBalls[j];
					}
				}
			} 

			
			
			for(int i = 0; i <noBalls; i++) {
				rxBalls[i] = rxBalls[i] + vxBalls[i]; 
				ryBalls[i] = ryBalls[i] + vyBalls[i]; 
			}




			for(int i = 0; i <noBalls; i++) {
				StdDraw.setPenColor(xColor[i]); 
				StdDraw.filledCircle(rxBalls[i], ryBalls[i], radius); 
			}


			StdDraw.show(pause); 



			

		} 

	}

}
