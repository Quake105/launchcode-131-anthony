package lab3;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);


		int noOfDieFaces  = ap.nextInt("How many faces does the die have?");
		int noOfDice = ap.nextInt("How many dice are there? ");
		int dThrows = ap.nextInt("How many times were the dice thrown? ");

		int[][] dValues = new int[dThrows][noOfDice];

		System.out.println("______________________________");
		for(int j = 0; j < dThrows; j++) {

			System.out.print("Throw " + (1+ j)+"  |");
			for(int i = 0; i < noOfDice; i++) {
				if(i < noOfDice) {
					dValues[j][i] = (int)( Math.random()* noOfDieFaces) +1;
				}

				System.out.print("   "+dValues[j][i] + "  ");
			}System.out.println();

		}

		System.out.println();
		System.out.println("_____________________");


		int[] sums = new int[dThrows];
		for(int n = 0; n < dThrows; n++) {
			System.out.print("Iteration "+ (n+1) + " Sum " + " |  ");


			for(int k = 0; k < noOfDice ; k++) {
				sums[n] += dValues[n][k] ;

			}	System.out.println( "  " + sums[n] + "  ");
		}	System.out.println();


		int[] sumArray = new int[noOfDice*noOfDieFaces];
		System.out.println("Sum______No of times Sum was seen_______");
		for(int a = 0; a < noOfDice*noOfDieFaces ; a++) {
			int sumC = 0;
			for(int c=0; c <dThrows; c++) {
				if(a ==sums[c]){
					sumC+=1;
				}
				//System.out.println(sumC);
			}sumArray[a] = sumC ;
			if(sumArray[a]==0){} else {
			System.out.println(a+ " |           " + sumArray[a] + "  ");
			}
		}
		System.out.println();
		
		/*int sumC = sums[n];
	int counter = 0;
	for(int g = 0; g < dThrows; g++) {

		if(sumC == sums[g]){
			counter +=1; 
		}
		sums[n] = counter;
	} */




		int sameThrow1=0;
		for(int j1 = 0; j1 < dThrows; j1++) {
			int sameThrow =0;
			for(int i = 0; i < noOfDice-1; i++) {

				if(dValues[j1][i] == dValues[j1][i+1]){
					sameThrow+=1;
				}
			}
			if(sameThrow / (noOfDice-1) == 1) {
				sameThrow1+=1;
			}
		}
		double percentSame = (double) sameThrow1 / dThrows;
		percentSame = (percentSame * 100);
		percentSame = (percentSame /100)*100;
		System.out.println();
		System.out.println("The fraction of dice throws showing the same value is "+(percentSame )+ "%");



	} 
}

