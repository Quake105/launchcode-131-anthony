package lab2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);

		int N = ap.nextInt("How many times should we play? ");
		//int var2 = ap.nextInt();
		//int var3 = ap.nextInt();
		double otherPlayer = 3;
		int otherPlayerWin = 0;
		int randPlayerWin = 0;

		for(int i = 0;  i < N; i++) {
			//System.out.println(i);
			double randPlayer = Math.random();
			if(randPlayer <= 1.0/3){
				System.out.println("Player 1 plays Rock");
				randPlayer = (int) 0;
			}
			else if 
			(randPlayer <= 2.0/3){
				System.out.println("Player 1 plays Paper");
				randPlayer = (int) 1;
			}
			else {
				System.out.println("Player 1 plays Scissors");
				randPlayer = (int) 2; 
			}
			
			
			if(otherPlayer % 3 == 0){
				System.out.println("Player 2 plays Rock");
			}
			else if 
			(otherPlayer % 3 == 1){
				System.out.println("Player 2 plays Paper");
			}
			else {
				System.out.println("Player 2 plays Scissors");
			}
			otherPlayer += 1;
			//System.out.println(randPlayer);
			//System.out.println(otherPlayer);
			//System.out.println(otherPlayer % 3);
			
			if((otherPlayer % 3 == 0) && randPlayer == 2) {
				otherPlayerWin += 1;
			}
			else if((otherPlayer % 3 == 1) && randPlayer == 0)  {
				otherPlayerWin += 1;
			}
			else if((otherPlayer % 3 == 2) && randPlayer == 1)  {
				otherPlayerWin += 1;
			}
			
			
			else if(randPlayer == 0 && (otherPlayer % 3 == 2) ) {
				randPlayerWin += 1;
			}
			else if(randPlayer == 1 && (otherPlayer % 3 == 0)) {
				randPlayerWin += 1;
			}
			else if(randPlayer == 2 && (otherPlayer % 3 == 1)) {
				randPlayerWin += 1;
			}
			//System.out.println(var2);
			//System.out.println(var3);
		}
		System.out.println("Out of a " + N +  " rounds.");
		System.out.println("Random Player won " + (double) randPlayerWin/N + " of the time.");
		System.out.println("Sequential Player won " + (double) otherPlayerWin/N + " of the time.");
	}

}
