package lab5;

public class Lab5Methods {

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static int sumDownBy2(int a) {
		int sum = 0;
		for(int i =0; i < a; i +=2) {
			sum += a-i;
		}
		return sum;
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static double harmonicSum(int a) {
		a = Math.abs(a);
		double sumH = 0;
		for(int i = 1; i < (a+1); i+=1) {
			sumH += (double) 1/ i;
		}
		return sumH;
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static double geometricSum(int a) {
		a = Math.abs(a);
		double geoSum = 0;
		for(int i = 0; i <= a; i++) {
			geoSum += 1/Math.pow(2,i);
		}
		return geoSum;

	}

	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */

	public static int multPos(int a, int b) {
		int sumM = 0;
		for(int i = 1; i <= b; i++) {
			sumM += a;
		}
		return sumM;
	}
/**
 * 
 * @param a
 * @param b
 * @return
 */
	public static int mult(int a, int b) {
		int multA = 0;
		

		if(a >= 0 && b >= 0) {
			multA = multPos(a,b); 

		}

		else if (a < 0 && b < 0) {
			a= -a;
			b=-b;
			multA = multPos(a,b); 

		}

		else if (a < 0 || b < 0) {
			if(b < 0) {
				a = -a;
				b = -b;
			}
 			for(int i = 1; i <= b; i++) {
				multA += a;
			}
		}
		return multA;
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int expt(int a, int b) {
		int exptA = 1;
		for(int i = 0; i < b; i++) {
			exptA = mult(exptA,a);
		}
		return exptA;
		
		
	}
	
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(sumDownBy2(7));
		System.out.println(harmonicSum(2));
		System.out.println(mult(-1,1));
	}
}
