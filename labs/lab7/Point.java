package lab7;

public class Point {
	private final double cordX;
	private final double cordY;

	public Point(double cordX, double cordY) {
		this.cordX = cordX;
		this.cordY = cordY;
	}

	public String toString (){
		return "" + "( " + this.cordX + " , " + this.cordY + " )";

	}

	public double getX() {
		return cordX;
	}


	public double getY() {
		return cordY;
	}

	public double distance(Point p){
		double distance =	Math.sqrt(Math.pow(cordX-p.cordX,2) + Math.pow(cordY-p.cordY,2));
		return distance;
	}

	public Point deflectX(){

		return new Point(-cordX, cordY);	
	}

	public Point deflectY(){

		return new Point(cordX, -cordY);	
	}
	
	public Point plus(Vector v){

		return new Point(cordX + v.getDeltaX(), cordY + v.getDeltaY());	
	}
	
	public Vector minus(Point p){

		return new Vector(cordX - p.getX(), cordY - p.getY());	
	}
	
	public Vector scale(double factor) {
		return new Vector(cordX * factor, cordY * factor);
	}

	

	public static void main(String args[]) {



	}

}
