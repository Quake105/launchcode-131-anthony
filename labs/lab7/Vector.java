package lab7;

public class Vector {

	private final double deltaX;
	private final double deltaY;

	public Vector(double deltaX, double deltaY) {
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}

	public String toString (){
		return "" + "[ " + this.deltaX + " , " + this.deltaY + " ]";

	}

	public double getDeltaX() {
		return deltaX;
	}


	public double getDeltaY() {
		return deltaY;
	}

	public double magnitude(){
		double magnitude =	Math.sqrt(Math.pow(deltaX,2) + Math.pow(deltaY,2));
		return magnitude;
	}

	public Vector deflectX(){

		return new Vector(-deltaX, deltaY);	
	}

	public Vector deflectY(){

		return new Vector(deltaX, -deltaY);	
	}
	
	public Vector plus(Vector v){

		return new Vector(deltaX + v.deltaX, deltaY + v.deltaY);	
	}
	
	public Vector minus(Vector v){

		return new Vector(deltaX - v.deltaX, deltaY - v.deltaY);	
	}
	
	public Vector scale(double factor) {
		return new Vector(deltaX * factor, deltaY * factor);
	}

	public Vector rescale(double magnitude) {
		 
		double a =  this.magnitude();
		if (deltaX == 0 && deltaY == 0  ){
			return new Vector (magnitude,0);

		}
		 return new Vector(magnitude * ((deltaX/a)), magnitude * (deltaY/a));
		
	}

	public static void main(String args[]) {

		Vector a1 = new Vector(4,3); 
		
		
		System.out.println(a1.toString());
		System.out.println(a1.magnitude());
	


	}
}
