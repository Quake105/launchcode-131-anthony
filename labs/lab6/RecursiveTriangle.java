package lab6;

import sedgewick.StdDraw;

public class RecursiveTriangle {
	public static double[] point(double x, double y) {
		double[] point = new double[2];
		x = point[0];
		y = point[1];
		return point;
	}

	public static double[] midPt(double[] a,  double[] b) {
		double[] midPt = new double[2];
		for(int i = 0; i < 2; i++ ) {
			midPt[i] = (a[i] + b[i])/2;
		}
		return midPt;
	}



	public static void tri(double[] a, double[] b, double[] c, double size) {
		
		if (size == 1) {
			double[] x = new double [3];
			double[] y = new double [3];

			x[0] = a[0];
			x[1] = b[0];
			x[2] = c[0];

			y[0] = a[1];
			y[1] = b[1];
			y[2] = c[1];

			StdDraw.filledPolygon(x,y);
		}
		else 
		{
			double[] d = new double[2];
			d = midPt(a,b);
			
			double[] e = new double[2];
			e = midPt(b,c);
			
			double[] f = new double[2];
			f = midPt(c,a);	
			
			tri(f,e,c, size - 1);
			tri(d,b,e, size - 1);
			tri(a,d,f, size - 1);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				double[] a = new double[2];
				a[0] = 0.0; 
				a[1] = 0.0;
				
				double[] b = new double[2];
				b[0] = 0.5; 
				b[1] = ((double) Math.sqrt(3)/2);
				
				double[] c = new double[2];
				c[0] = 1.0; 
				c[1] = 0.0; 
						
				tri(a,b,c,10);

		}
	}

