package lab1;
import cse131.ArgsProcessor;
public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
ArgsProcessor ap = new ArgsProcessor(args);
String name = ap.nextString("The name of this food: ");
double carbs = ap.nextDouble("The number of grams of carbohydrates in this food: ");
double fat = ap.nextDouble("The number of grams of fat in this food: ");
double proteins = ap.nextDouble("The number of grams of protein in this food: ");
int statedCals = ap.nextInt("The number of stated calories on the lable of this food: ");

double carbCals =  carbs * 4;
double fatCals =  fat * 9;
double proteinCals = proteins * 4 ;
double totalCalcCals = (carbCals + fatCals + proteinCals);
double fibreCals = totalCalcCals - statedCals;
double fibre = fibreCals / (4);
int fibre1 = (int) Math.round(fibre*100);
fibre = fibre1 / 100.0;
int fibreCals1 = (int) Math.round(fibreCals*10);
fibreCals = fibreCals1 / 10.0;

double percentCarbs = carbCals / statedCals;
int percentCarbs1 = (int) Math.round(percentCarbs*1000);
percentCarbs = percentCarbs1 / 10.0;
double percentFat = fatCals / statedCals;
int percentFat1 = (int) Math.round(percentFat*1000);
percentFat = percentFat1 / 10.0;
double percentProtein = proteinCals / statedCals;
int percentProtein1 = (int) Math.round(percentProtein*1000);
percentProtein = percentProtein1 / 10.0;

boolean lowCarb = carbs / (carbs + fat + proteins) < 0.25;
boolean lowFat = fat / (carbs + fat + proteins) < 0.15;
boolean heads = Math.random() < 0.5;

System.out.println("This food is said to have " + statedCals + " (available) Calories." 
 + '\n' + "With " +  fibreCals + " unavailable Calories, this food has " + fibre + 
 " grams of fiber"

 + '\n' + '\n' + "Approximately, " + '\n' + '\t' + percentCarbs + "% of your food is carbohydrates" 
 + '\n' + '\t'+ percentFat + "% of your food is fat" + '\n' + '\t' 
 + percentProtein +  "% of your food is protein" + '\n' + '\n' +

 "This food is acceptable for a low-carb diet?   " +  lowCarb + '\n' +
 "This food is acceptable for a low-fat diet?   " +  lowFat + '\n' +
 "By coin flip, you should eat this food?   " +  heads);


	}

}
